function openLink(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("caja");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("bo");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
    // Click on the first tablink on load
  }
  